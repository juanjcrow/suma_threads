#include <pthread.h>
#include <stdio.h>

#define SIZE_ARRAY 100
#define NUM_THREADS 20
#define SIZE_BLOCK SIZE_ARRAY/NUM_THREADS

int a[SIZE_ARRAY];
volatile int parallelSum;

void initialize_array();
int serialSum(int *,int);
void *concurrentSum(void *);

int main()
{
	parallelSum = 0;
	initialize_array();
	printf("Suma serial: %d\n",serialSum(a,SIZE_ARRAY));
	
	pthread_t threads[NUM_THREADS];
	int block_start[NUM_THREADS];

	/************/
	
	for (int i = 0; i < NUM_THREADS; i++){
		block_start[i] = i * SIZE_BLOCK;
		pthread_create(&threads[i], NULL, concurrentSum, &block_start[i]);
		pthread_join(threads[i], NULL);
	}

	/***********/

	printf("Suma paralela: %d\n",parallelSum);
		
}

void initialize_array()
{
	for(int i=0;i<SIZE_ARRAY;i++)
		a[i] = 2*i;
}

int serialSum(int *a, int size)
{
	int sum = 0;
	for(int i=0;i<size;i++)
		sum = sum + a[i];

	return sum;
}

void *concurrentSum(void *start)
{
	int inicio = *(int *)start;
	for (int i = inicio; i<(inicio+SIZE_BLOCK);i++){
		parallelSum += a[i];
	}
	return NULL;		
}
